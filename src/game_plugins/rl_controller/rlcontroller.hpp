// This file is part of Dust Racing 2D.
// Copyright (C) 2012 Jussi Lind <jussi.lind@iki.fi>
//
// Dust Racing 2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Dust Racing 2D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Dust Racing 2D. If not, see <http://www.gnu.org/licenses/>.

#ifndef RLCONTROLLER_HPP
#define RLCONTROLLER_HPP

#include <pidcontroller.hpp>
#include <Rlearner/WorldBase.h>
#include <Rlearner/rewardfunction.h>
#include <Rlearner/discretizer.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/qfunction.h>
#include <Rlearner/policies.h>

using namespace rlearner;

class CarReward: public RewardFunction {
public:
	virtual RealType getReward(
		StateCollection *oldState,
		Action *action,
        StateCollection *newState
	);

	virtual ~CarReward() = default;
};

//! A controller based on Rlearner.
class RLController: public PIDController, public WorldBase {
private:
	CarReward _rewardFunction;

	shared_ptr<AbstractStateDiscretizer> _discretizer;
	shared_ptr<FeatureQFunction> _qTable;
	shared_ptr<TDLearner> _learner;
	shared_ptr<AgentController> _policy;

public:
	enum {
		angularError = 0,
		angularDeltaError,
		angularDeltaError2,
		distanceError,
		distanceDeltaError,
		distanceDeltaError2
	};

protected:
	virtual void writeState(State* state) override;

    //! Steering logic.
    virtual float steerControl(bool isRaceCompleted);
    //! Brake/accelerate logic.
	virtual float speedControl(bool isRaceCompleted);

public:
	RLController(Car& car);
	virtual ~RLController();
};

#endif // RLCONTROLLER_HPP
