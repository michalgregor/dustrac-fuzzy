#include <iostream>
#include <aifactory.hpp>

#include "rlcontroller.hpp"
#include "loader.hpp"

#include <QCommandLineOption>
#include <QCommandLineParser>

std::shared_ptr<PluginInfo> pluginInfo() {
	auto info = std::make_shared<PluginInfo>();
	info->name = "RLController";
	return info;
}

void init(Game&, const PluginInfo& info, const QStringList& args) {
	AIFactory& factory = AIFactory::instance();
	factory.add("animus",
		[](Car& car) {
			return new RLController(car);
		}
	);
}
