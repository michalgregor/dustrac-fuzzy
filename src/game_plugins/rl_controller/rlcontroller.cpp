#include <rlcontroller.hpp>
#include <car.hpp>
#include <fstream>

RealType CarReward::getReward(
	StateCollection* oldStateCol,
	Action* /*action*/,
    StateCollection* newStateCol
) {
	auto* oldState = oldStateCol->getState();
	auto* newState = newStateCol->getState();

	RealType prevTargetDistance = oldState->getContinuousState(RLController::distanceError);
	RealType targetDistance = newState->getContinuousState(RLController::distanceError);

	RealType reward = prevTargetDistance - targetDistance;
	reward -= std::abs(newState->getContinuousState(RLController::angularError));
	reward -= std::abs(RLController::angularDeltaError);

	return reward;
}

void RLController::writeState(State* state) {
	state->setContinuousState(RLController::angularError, m_data.angularErrors.error);	
	state->setContinuousState(RLController::angularDeltaError, m_data.angularErrors.deltaError);
	state->setContinuousState(RLController::angularDeltaError2, m_data.angularErrors.deltaError2);
	state->setContinuousState(RLController::distanceError, m_data.distanceErrors.error);	
	state->setContinuousState(RLController::distanceDeltaError, m_data.distanceErrors.deltaError);
	state->setContinuousState(RLController::distanceDeltaError2, m_data.distanceErrors.deltaError2);
}

float RLController::steerControl(bool) {
	auto& actions = makeStep();
	return static_cast<NumericAction*>(actions.front())->getValue();
}

float RLController::speedControl(bool) {
	return 60;
}

RLController::RLController(Car& car):
	PIDController(car, false), WorldBase(new StateProperties(6, 0))
{
	auto agent = make_shared<Agent>(_currentState->getStateProperties());

	agent->addAction(new NumericAction(-7));
	agent->addAction(new NumericAction(-5));
	agent->addAction(new NumericAction(-1));
	agent->addAction(new NumericAction(0));
	agent->addAction(new NumericAction(1));
	agent->addAction(new NumericAction(5));
	agent->addAction(new NumericAction(7));

	addAgent(agent);

	std::vector<RealType> partition = {-45, -40, -35, -30, -25, -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40};
	_discretizer = make_shared<SingleStateDiscretizer>(RLController::angularError, std::move(partition));
	agent->addStateModifier(_discretizer.get());

	_qTable = make_shared<FeatureQFunction>(agent->getActions(), _discretizer.get());
	_learner = make_shared<QLearner>(&_rewardFunction, _qTable.get());
	_learner->setParameter("QLearningRate", 0.1);
	_learner->setParameter("ReplacingETraces", 1.0);
	_learner->setParameter("Lambda", 0.95);
	_learner->setParameter("DiscountFactor", 0.95);

	agent->addSemiMDPListener(_learner.get());

	_policy = make_shared<QStochasticPolicy>(agent->getActions(),
	    new EpsilonGreedyDistribution(0.1, true), _qTable.get());
	agent->setController(_policy.get());

	std::ifstream file("qtable.dat");
	if(file.good()) {
		_qTable->loadData(file);
	}

	std::ofstream file2("qtable2.dat");
	_qTable->saveData(file2);
}

RLController::~RLController() {
	std::ofstream file("qtable.dat");
	_qTable->saveData(file);
}

